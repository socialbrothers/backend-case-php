<?php

require_once 'Database.php';

class Category
{
    public static function getAll()
    {
        $db = Database::getInstance();
        $res = $db->query('SELECT * FROM categories');
        if (count($res) > 0) {
            return $res;
        }

        return [];
    }
}