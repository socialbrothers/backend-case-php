# Backend Case | PHP
This case is a simple API setup without using a framework. Use a REST client of you own preference to communicate with the API. The API expects to be hosted on an Apache/MySQL/PHP environment.
## Assignment 1
* Fix / secure the user authentication
* Fix / secure the database interaction
## Assignment 2
* Use a recursive method to list all categories structured with children
## Assignment 3
* How can this framework be improved?