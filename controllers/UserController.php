<?php

require_once 'models/User.php';

class UserController
{
    public function post($data)
    {
        $user = User::login($data['email'], $data['password']);
        if ($user) {
            return json_encode($user);
        }

        return json_encode(['todo' => 'Assignment 1: User Authentication']);
    }

    public function get($data)
    {
        return null;
    }
}
